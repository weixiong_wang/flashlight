package com.doubleboy.flashlight;

import android.app.Application;
import android.content.res.Configuration;
import android.hardware.Camera;
import android.os.Handler;

import com.doubleboy.flashlight.lang.L;

public class MyApplication extends Application {
    private Camera camera = null;
    private static MyApplication myAppliaction = null;

    public static MyApplication getMyApplication() {
        return myAppliaction;
    }

    public Camera getCamera() {
        return camera;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        //模拟预加载闪光灯
		camera = Camera.open();
		// Parameters params = camera.getParameters();
		// params.setFlashMode(Parameters.FLASH_MODE_OFF);
		// camera.setParameters(params);
		// camera.startPreview();
        myAppliaction = this;
        L.d(null);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        L.d(null);
    }

    /**
     * 将应用程序从进程中杀死
     */
    public void exit() {
        camera.setPreviewCallback(null);
        camera.release(); // 关掉照相机,并且释放资源
        camera = null;
        try {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
            android.os.Process.killProcess(android.os.Process.myPid());
            System.exit(0);
                }
            }, 500);
        } catch (Exception e) {

        }
    }
}

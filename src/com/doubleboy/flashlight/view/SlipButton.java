package com.doubleboy.flashlight.view;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;

import com.doubleboy.flashlight.R;
import com.doubleboy.flashlight.lang.L;
@Deprecated
public class SlipButton extends View implements OnTouchListener {

    private boolean isChanged = true;
    private boolean animating = false;
    private boolean isOpen;

    private boolean isMoveEnd = true;

    private Rect btn_On, btn_Off;

    private OnChangedListener changListener;// 这是一个监听事件类

    private Bitmap bg_on, bg_off, bg_switch;

    public SlipButton(Context context) {
        super(context);
        init();
    }

    public SlipButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public SlipButton(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    int wsp;

    private Matrix matrix;

    private Paint paint;

    private void init() {
        matrix = new Matrix();
        paint = new Paint();
        bg_on = BitmapFactory.decodeResource(getResources(),
                R.drawable.flashlight_on);
        bg_off = BitmapFactory.decodeResource(getResources(),
                R.drawable.flashlight_bg);
        bg_switch = BitmapFactory.decodeResource(getResources(),
                R.drawable.flashlight_button);
        setOnTouchListener(this);
        wsp = (bg_off.getWidth() - bg_switch.getWidth()) / 2;
        this.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                SlipButton.this.onClick();
            }
        });
        nowY = bg_on.getHeight() - bg_switch.getHeight();
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int w = measuredWidth();
        int h = measuredHeight();
        setMeasuredDimension(w, h);
    }

    private int measuredHeight() {
        return bg_off.getHeight();
    }

    private int measuredWidth() {
        return bg_off.getWidth();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (animating) {
            return;
        }
        if (isOpen) {
            canvas.drawBitmap(bg_on, matrix, paint);
            if (isMoveEnd) {
                canvas.drawBitmap(bg_switch, wsp, 0, paint);
            } else {
                canvas.drawBitmap(bg_switch, wsp, nowPostion, paint);
            }
        } else {
            canvas.drawBitmap(bg_off, matrix, paint);
            if (isMoveEnd) {
                canvas.drawBitmap(bg_switch, wsp, bg_off.getHeight()
                        - bg_switch.getHeight(), paint);
            } else {
                canvas.drawBitmap(bg_switch, wsp, nowPostion, paint);
            }
        }
    }

    float nowPostion, nowY, oldY, moveY = -1;

    public boolean onTouch(View v, MotionEvent event) {
        isMoveEnd = false;// 每次进来的时候默认都是没有移动结束
        switch (event.getAction()) {
            case MotionEvent.ACTION_MOVE: // move事件只是进行开关图片的移动
                float initHeight = bg_on.getHeight() - bg_switch.getHeight();
                if (event.getY() < 0) {
                    nowPostion = 0;
                } else if ((!isOpen && event.getY() > initHeight) || (isOpen && event.getY() < bg_switch.getHeight())) {
                    moveY = event.getY() - oldY;
                    L.d("ACTION_MOVE: now:%S,old:%S,move:%S", event.getY(), oldY,
                            moveY);
                    oldY = event.getY();
                    nowPostion += moveY;
                    if (nowPostion > initHeight)
                        nowPostion = initHeight;
                    else if (nowPostion < initHeight / 3) {
                        nowPostion = 0;
                    }
                } else
                    nowPostion = event.getY();
                break;
            case MotionEvent.ACTION_DOWN:
                Log.i("mainActivity", "ACTION_DOWN:" + event.getY());
                if (event.getX() > bg_on.getWidth()
                        || event.getY() > bg_on.getHeight())
                    return false;
                oldY = event.getY();
                break;
            case MotionEvent.ACTION_UP: // 手松开事件是进行闪光灯具体的开关
                isMoveEnd = true;// 开关移动结束
                boolean currentStatus = isOpen;
                Log.i("mainActivity", "ACTION_UP:" + event.getY());
                if (event.getY() <= 0 || nowPostion == 0) {
                    nowPostion = 0;
                    isOpen = true;
                } else if (event.getY() > bg_on.getHeight() - bg_switch.getHeight()) {
                    nowPostion = bg_on.getHeight() - bg_switch.getHeight();
                    isOpen = false;
                } else if (event.getY() > 0
                        && event.getY() < (bg_on.getHeight() - bg_switch
                        .getHeight()) / 2) {
                    nowPostion = event.getY();
                    isOpen = true;
                } else {
                    nowPostion = event.getY();
                    isOpen = false;
                }
                if (currentStatus != isOpen)
                    changListener.OnChanged(isOpen);
                break;
            default:
        }
        invalidate();// 调用重绘
        return true;
    }

    public void SetOnChangedListener(OnChangedListener changListener) {
        this.changListener = changListener;
        isOpen = false;// 在这里可以初始化手电筒首次开启时是打开还是关闭状态
        changListener.OnChanged(isOpen);
    }

    @SuppressLint("NewApi")
    public void onClick() {
        this.animate().start();
    }

    @Override
    protected void onAnimationStart() {
        super.onAnimationStart();
        this.animating = true;
    }

    @Override
    protected void onAnimationEnd() {
        super.onAnimationEnd();
        this.animating = false;
    }

    // 声明监听类
    public interface OnChangedListener {
        abstract void OnChanged(boolean CheckState);
    }
}

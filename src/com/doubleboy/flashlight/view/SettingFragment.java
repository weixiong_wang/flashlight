package com.doubleboy.flashlight.view;

import android.app.FragmentTransaction;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.preference.PreferenceScreen;

import com.doubleboy.flashlight.R;

/**
 * Created by xzz on 14-3-19.
 */
public class SettingFragment extends PreferenceFragment {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.set_perference);
    }
    
    @Override
    public void onStart() {
    	super.onStart();
    	getActivity().getActionBar().setTitle(R.string.settings_title);
    }

	@Override
	public boolean onPreferenceTreeClick(PreferenceScreen preferenceScreen,
			Preference preference) {
		if("ABOUT_ME".equalsIgnoreCase(preference.getKey())){
			getActivity().getFragmentManager()
				.beginTransaction()
				.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
				.replace(android.R.id.content, new AboutFragment())
				.addToBackStack(null)
				.commit();
			return true;
		}
		return false;
	}
	
	public static class AboutFragment extends PreferenceFragment{
		@Override
		public void onCreate(Bundle savedInstanceState) {
			super.onCreate(savedInstanceState);
			addPreferencesFromResource(R.xml.set_about_me);
		}
	    
	    @Override
	    public void onStart() {
	    	super.onStart();
	    	getActivity().getActionBar().setTitle(R.string.about_me);
	    }
	}
}

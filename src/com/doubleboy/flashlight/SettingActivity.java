package com.doubleboy.flashlight;

import android.app.Activity;
import android.os.Bundle;

import com.doubleboy.flashlight.view.SettingFragment;

/**
 * Created by xzz on 14-3-19.
 */
public class SettingActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getFragmentManager().beginTransaction()
                .replace(android.R.id.content, new SettingFragment())
                .commit();
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
    }
}

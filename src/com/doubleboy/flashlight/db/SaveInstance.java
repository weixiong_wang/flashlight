package com.doubleboy.flashlight.db;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.doubleboy.flashlight.lang.L;

/**
 * Created by xzz on 14-3-18.
 */
public class SaveInstance {
    /**
     * 屏幕状态
     */
    public static final String SCREEN_STATUS = "screen_status";
    /**
     * 首次打开应用
     */
    public static final String IS_FIRST = "is_first";

    public boolean getScreenStatus(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("lightSetting", Context.MODE_PRIVATE);
        return sharedPreferences.getBoolean(SCREEN_STATUS, true);
    }

    public void setScreenStatu(Context context, boolean isLight) {
        saveInstance(context, SCREEN_STATUS, isLight);
        L.d(":" + isLight);
    }

    public boolean getAutoOn(Context context) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPreferences.getBoolean("auto_on", false);
    }

    public boolean getAutoOff(Context context) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPreferences.getBoolean("auto_off", false);
    }

    /**
     * 是否是第一次使用。是的话返回true 并自动设置成false
     *
     * @param context 。。
     * @return 是否是第一次使用
     */
    public boolean isFirstOpen(Context context) {
        boolean isFirst;
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        isFirst = sharedPreferences.getBoolean(IS_FIRST, true);
        if (isFirst) {
            SharedPreferences.Editor editor = sharedPreferences.edit();//获取编辑器
            editor.putBoolean(IS_FIRST, false);
            editor.commit();//提交修改
        }
        return isFirst;
    }

    private void saveInstance(Context context, String key, boolean value) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("lightSetting", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();//获取编辑器
        editor.putBoolean(key, value);
        editor.commit();//提交修改
    }
}
